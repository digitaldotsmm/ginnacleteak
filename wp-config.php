<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */
 switch ($_SERVER['HTTP_HOST']) {    
    case '192.168.1.126':
    case 'teak.dd':   
        define('DB_NAME', 'teak');
        define('DB_USER', 'webuser');
        define('DB_PASSWORD', 'nopassword');
        define('DB_HOST', '192.168.1.200');
        break;
    case 'ginnacleteak.myanmarcafe.info':
        define('DB_NAME', 'digitald_teak');
        define('DB_USER', 'digitald_webuser');
        define('DB_PASSWORD', '9UrEbO{g0Ck3');
        define('DB_HOST', 'localhost');
        break;
    default :
        define('DB_NAME', 'teak_production');
        define('DB_USER', 'teak_webuser');
        define('DB_PASSWORD', '@#}TK_;nS30C');
        define('DB_HOST', 'localhost');
        break;
}
$protocol = (!empty($_SERVER['HTTPS']) ) ? 'https://' : 'http://';
define('WP_SITEURL', $protocol . $_SERVER['HTTP_HOST']);
define('WP_HOME', $protocol . $_SERVER['HTTP_HOST']);
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';l0)$<T+wgjD qRBCu7(pch]A4!|_VJ?R$soT_Kmi~VZmUYg9c;7l87TTqZp4rHy');
define('SECURE_AUTH_KEY',  '~,dRm;birkQYu#$HefSr-N(5+n0W+|ZYxJq`+Y,W@tzc7nMw%7/N,9R>LeWSJX=9');
define('LOGGED_IN_KEY',    '|&@@7[Mv2g]Fi7AF:w+k;!X|pvYSA:.F];<9pkyBj*?7Oc3U*{+MN`yQ9q=ZYB8A');
define('NONCE_KEY',        'eQJ8LSg_svenYUxw-!KCER>E0+-)YE7-iI+xL+(*M1-l{,b$~lad|CZ-Q|U|G|#z');
define('AUTH_SALT',        'xbnOqn3-qqL;26#|bM5eUueM90(){@CI&(-=6Z?4BgS|7jabapsXqeI?r2#Exs:#');
define('SECURE_AUTH_SALT', 'Mw,zk;oVcgy#[$?Fr$<Kj>@[jdO_6, @!n_B$/}|sC4DZ>3x/aQfjoO%L;o,XS]]');
define('LOGGED_IN_SALT',   'bx@p0nAs4Qkn#^hspD,%FOz OGZ|a+~oXs^>9yWi-`01Vld0tjt&6|]3o@O?gx`E');
define('NONCE_SALT',       '{zoQ>7O0S9>M)Vi5h0slBDWlRjflhtqZA_~>%xB1kfK1*<k$iHhx-q0`/a`af4BH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */

define('AUTOSAVE_INTERVAL', 180);  
define( 'WP_AUTO_UPDATE_CORE', false);
define( 'WP_POST_REVISIONS', 3 );
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
