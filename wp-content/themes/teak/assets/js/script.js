/*!
 *      Author: DigitalDots
	name: script.js	
	requires: jquery	
 */

var ddFn = {
    
    init: function(){
        Global = new this.Global();
        
        Global.__init();        
    },
    

    Global:function(){        
        
        this.__init = function(){
            //prettyPhoto
//             $("a[rel^='prettyPhoto']").prettyPhoto({
//                 show_title: false
//             });
            // add placeholder
            $(".es_textbox_class").attr("placeholder", "Enter your email....");
            $('#featured-slider').bxSlider({
		  minSlides: 1,
		  maxSlides: 4,
		  slideWidth: 292,
                  slideMargin: 0,
		  nextSelector: '.arr-up-down-next',
		  prevSelector: '.arr-up-down-prev',
		  nextText: '<i class="fa fa-angle-right"></i>',
		  prevText: '<i class="fa fa-angle-left"></i>'
            });  
            $('#main-slider').flexslider({
                animation: "slide"
            });
            $(".mask").css("opacity","0");
            $(".mask").hover(function () {
                $(this).stop().animate({
                opacity: .7
                }, "slow");
            },
            function () {
                $(this).stop().animate({
                opacity: 0
                }, "slow");
            });
            
            (function() {
 
            // store the slider in a local variable
            var $window = $(window),
                flexslider = { vars:{} };

            // tiny helper function to add breakpoints
            function getGridSize() {
              return (window.innerWidth < 600) ? 2 :
                     (window.innerWidth < 900) ? 3 : 4;
            }

            $window.load(function() {
              $('#featured-slider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 285,
                itemMargin: 10,
                minItems: getGridSize(), // use function to pull in initial value
                maxItems: getGridSize() // use function to pull in initial value
              });
            });

            // check grid size on resize event
            $window.resize(function() {
              var gridSize = getGridSize();

              flexslider.vars.minItems = gridSize;
              flexslider.vars.maxItems = gridSize;
            });
          }());
        }
      
        this.home_url = function(){
            var home_page_url = window.location.protocol + "//" + window.location.host + "/";     
            return home_page_url;
        }
        this.template_url = function(){
            return this.home_url() + 'wp-content/themes/teak/';           
        }        
        this.ajax_url = function(){
            var ajax_url = this.home_url() + "wp-admin/admin-ajax.php";        
            return ajax_url;
        }              
    } // end of Global
};


var Global;

$ = $.noConflict();
$(document).ready(function(){  
    ddFn.init();
});