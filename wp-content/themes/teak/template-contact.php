<?php

/*
 * Template Name: Contact Us
 */
?>
<?php global $THEME_OPTIONS; get_header(); ?>
<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
    <h2>
        Our Office Location
    </h2>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <iframe src="https://www.google.com/maps/d/embed?mid=1Sl_Q2XI2pFduimtGP0ruSBjgurI&z=13" width="100%" height="400"></iframe>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <table class="margintop10">
                <tbody>
                   <?php if($THEME_OPTIONS['info_address']): ?>
                      <tr class="row">
                         <td class="footericon"><i class="fa fa-map-marker"></i></td>
                         <td class="footerfact"><?php echo nl2br($THEME_OPTIONS['info_address']); ?></td>
                      </tr>
                   <?php endif; ?>
                   <?php if($THEME_OPTIONS['info_phone']): ?>
                      <tr class="row">
                         <td class="footericon"><i class="fa fa-phone"></i></td>
                         <td class="footerfact"><?php echo $THEME_OPTIONS['info_phone']; ?></td>
                      </tr>
                   <?php endif; ?>
                  <?php if($THEME_OPTIONS['info_burma_phone']): ?>
                      <tr class="row">
                         <td class="footericon"><i class="fa fa-phone"></i></td>
                         <td class="footerfact"><?php echo $THEME_OPTIONS['info_burma_phone']; ?></td>
                      </tr>
                   <?php endif; ?>
                   <?php if($THEME_OPTIONS['info_fax']): ?>
                      <tr class="row">
                         <td class="footericon"><i class="fa fa-fax"></i></td>
                         <td class="footerfact"><?php echo $THEME_OPTIONS['info_fax']; ?></td>
                      </tr>
                   <?php endif; ?>
                   <?php if($THEME_OPTIONS['info_email']): ?>
                      <tr class="row">
                         <td class="footericon"><i class="fa fa-envelope   "></i></td>
                         <td class="footerfact">
                             <?php if( strpos($THEME_OPTIONS['info_email'], ',') ): ?>
                                  <?php $emails = explode(',', $THEME_OPTIONS['info_email']); ?>
                                  <?php foreach( $emails as $email ): ?>
                                     <a href="mailto:<?php echo $email; ?>">
                                         <?php echo $email; ?>
                                     </a><br/>
                                  <?php endforeach; ?>
                             <?php else: ?>                                               
                             <a href="mailto:<?php echo $THEME_OPTIONS['info_email']; ?>">
                                 <?php echo $THEME_OPTIONS['info_email']; ?>
                             </a>
                             <?php endif; ?>
                          </td>
                      </tr>
                   <?php endif; ?>
                      <tr class="row">
                         <td class="footericon"><i class="fa fa-globe"></i></td>
                         <td class="footerfact">www.teak.net</td>
                      </tr>
                </tbody>
             </table>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <h2>
        Please contact us by following form.
    </h2>
    <?php echo do_shortcode('[contact-form-7 id="38" title="Contact Us"]'); ?>
</div>
 <?php get_footer(); ?>