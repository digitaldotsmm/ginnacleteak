                <?php if(!is_front_page() && !is_home()): ?>
                            </div>
                        </div>
                    </div><!-- end main-section -->
                <?php endif; ?>
                <div id="footer">
                    <div id="top-footer" class="row  paddingtop20 paddingbottom20 bg-eaeaea">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            <h2 class="text-uppercase">Subscribe here</h2>
                            <div id="subscriber">
                                <?php
                                    dynamic_sidebar('main-sidebar-widget-area'); 
                                ?>
                            </div>
                        </div>
                    </div><!-- end top-footer -->
                    <div id="bottom-footer" class="row">
                        <div class="col-xs-5 col-sm-4 col-md-6 col-lg-6">
                            <p class="margintop10 marginbottom10">
                                Copyright &copy; <?php echo date("Y"); ?> All Rights Reserved.
                            </p>
                        </div>
                        <div class="col-xs-7 col-sm-8 col-md-6 col-lg-6">
                            <ul class="margintop10 marginbottom10">
                                <li>
                                    Created by :
                                    <a href="//digitaldots.com.mm/" target="_blank" title="Yangon's Creative Digital Agency">
                                        <img src="<?php echo ASSET_URL; ?>images/digitaldots.png" alt="Yangon's Creative Digital Agency">
                                    </a>
                                </li>
                                <?php if($THEME_OPTIONS['goldenplaceid']): ?>
                                    <li>
                                        <span>Find us on :</span>
                                        <a href="<?php echo $THEME_OPTIONS['goldenplaceid']; ?>" target="_blank" title="Yangon's Business Profile">
                                            <img src="<?php echo ASSET_URL; ?>images/goldenplace.png" alt="Yangon's Business Profile">
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div><!-- end bottom-footer -->
                </div><!-- end footer -->
            </div><!-- end main-content -->
        <?php wp_footer(); ?>
    </body>
</html>