<?php global $THEME_OPTIONS; ?>
<!doctype html>
<!--[if lt IE 7 ]>	<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>		<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>		<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>		<html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en"  class="no-js">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<title><?php wp_title(''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<?php if ( file_exists(TEMPLATEPATH .'/favicon.png') ) : ?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png">
<?php endif; ?>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<?php $body_classes = join( ' ', get_body_class() ); ?>
<body class="<?php if( !is_search() )echo $body_classes; global $THEME_OPTIONS; ?>">
<div id="main-content" class="container margintop50 marginbottom50"><!-- start main-content -->
    <div id="header">     
        <div class="row">
            <div id="top-header" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ul class="contact-info margintop10 marginbottom10">
                    <?php if($THEME_OPTIONS['info_phone']): ?>
                        <li>
                            <?php echo "<i>CALL US:</i>" . $THEME_OPTIONS['info_phone']; ?>
                        </li>
                    <?php endif; ?>
                    <?php if($THEME_OPTIONS['info_burma_phone']): ?>
                        <li>
                            <?php echo "<i>CALL US:</i>" . $THEME_OPTIONS['info_burma_phone']; ?>
                        </li>
                    <?php endif; ?>
                    
                    <?php if($THEME_OPTIONS['info_fax']): ?>
                        <li>
                            <?php echo "<i>FAX:</i>" . $THEME_OPTIONS['info_fax']; ?>
                        </li>
                    <?php endif; ?>
                    <?php $emails = $THEME_OPTIONS['info_email']; if($emails): ?>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <?php
                                    if (strpos($emails, ',')):
                                        $emails = explode(',', str_replace(' ', '', $emails));
                                        foreach($emails as $email):
                            ?>
                                <a href="mailto:<?php echo $email; ?>">
                                    <?php echo $email; ?>
                                </a>
                            <?php
                                        endforeach;
                                    endif;
                            ?>
                        </li>
                    <?php endif; ?>
                </ul><!-- end of contact-info -->
            </div><!-- end of top-header -->
        </div>
        <div class="row">
            <div id="logo-section" class="col-md-12 paddingtop10 paddingbottom10">
                <div id="logo" class="col-xs-8 col-sm-6 col-md-6 col-lg-6">
                    <a href="<?php echo WP_HOME; ?>">
                        <img src="<?php echo ASSET_URL; ?>images/logo.png" alt="<?php echo bloginfo(); ?>" class="img-responsive">
                    </a>
                </div><!-- end of logo -->
                <div id="social" class="col-xs-4 col-sm-6 col-md-6 col-lg-6 margintop20 marginbottom20">
                    <span>Please follow us on social media!</span>
                    <ul>
                        <?php if($THEME_OPTIONS['facebookid']): ?>
                            <li>
                                <a href="<?php echo $THEME_OPTIONS['facebookid']; ?>" target="_blank">
                                    <img src="<?php echo ASSET_URL; ?>images/fb.png" alt="Facebook Page">
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if($THEME_OPTIONS['twitterid']): ?>
                            <li>
                                <a href="<?php echo $THEME_OPTIONS['twitterid']; ?>" target="_blank">
                                    <img src="<?php echo ASSET_URL; ?>images/twitter.png" alt="Twitter Page">
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- end header -->
    
    <div id="menu-section">
        <div class="row">
            <a class="navbar-brand visible-xs visible-sm" href="#">Menu</a>
            <button aria-controls="mobile-menu" aria-expanded="false" class="collapsed navbar-toggle" data-target="#mobile-menu" data-toggle="collapse" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <div id="mobile-menu" class="col-md-12 collapse navbar-collapse">
                <?php 
                    $args = array( 
                                   'theme_location'  => 'mobile',
                                   'container'       => FALSE,
                                   'menu_class'      => 'nav navbar-nav hidden-md hidden-lg',
                               );
                    wp_nav_menu($args); 
                ?>
            </div>
        </div>
        <div class="row">
            <div id="main-menu" class="col-md-12">
                <div class="title text-uppercase fontmontserr hidden-xs hidden-sm">
                    Featured Lists
                </div>
                <?php 
                    $args = array( 
                                   'theme_location'  => 'main',
                                   'container'       => FALSE,
                                   'menu_class'      => 'nav navbar-nav hidden-xs hidden-sm',
                               );
                    wp_nav_menu($args); 
                ?>
                
            </div>
        </div>
        <div class="row">
            <div id="featured-menu-slide" class="col-md-12">
                <div id="featured-menu-section" class="hidden-xs hidden-sm">
                    <?php 
                        $args = array(
                                'posts_per_page'   => -1,
                                'orderby'          => 'date',
                                'order'            => 'ASC',
                                'post_type'        => GT_TYPE_PRODUCT,
                                'post_status'      => 'publish',
                        );
                        $products = get_posts($args); 
                        if($products):
                    ?>
                    <ul>
                        <?php foreach($products as $product): ?>
                            <li>
                                <a href="<?php echo get_permalink($product->ID); ?>"class="text-uppercase font-opensans margintop25 marginbottom25">
                                    <?php echo $product->post_title; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>  
                </div>
                <div id="slide-section">
                    <?php 
                        $slides = get_field('slide', GT_HOME, true); 
                        if($slides):
                    ?>
                    <div id="main-slider" class="flexslider">
                        <ul class="slides">
                            <?php 
                                foreach($slides as $slide):
                                    $resize_img =aq_resize($slide['url'],844,351,true,true,true); 
                            ?>
                                <li class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    <img src="<?php echo $resize_img; ?>" alt="<?php echo $slide['title']; ?>" title="<?php echo $slide['title']; ?>">
                                    <p class="flex-caption"><?php echo $slide['caption']; ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div><!-- end main-slider -->
                    <?php endif; ?>
                </div><!-- end slide-section -->
            </div>
        </div>
    </div><!-- end menu-section -->
    
    <?php if(!is_front_page() && !is_home()): ?>
        <div id="main-section">
            <div class="row">
                <div class="col-md-12 paddingtop30 paddingbottom30">
                    <h1 class="main-title text-center marginbottom30">
                        <?php
                            if(is_archive()){
                                $title = "Product Listing";
                            }elseif(is_404()){
                                $title = "Page not found!";
                            }elseif(is_single() || is_page()){
                                $title = $post->post_title;
                            }
                            echo $title; 
                        ?>
                    </h1>
    <?php endif; ?>