<?php get_header(); ?>
<div class="row">
    <div class="col-md-12">
        <?php echo apply_filters("the_content",$post->post_content,TRUE); ?>
    </div>
</div>
<?php 
    $gallery = get_field('gallery',$post->ID,true); 
    if($gallery):
?>
<div class="row">
<?php foreach ($gallery as $img): ?>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <div class="wp-caption marginbottom30">
            <img class="img-responsive img-thumbnail" src="<?php echo $img['sizes']['medium']; ?>" alt="<?php echo $img['title']; ?>">
            <?php if($img['caption']): ?>
                <p class="wp-caption-text">
                    <?php echo $img['caption']; ?>
                </p>
            <?php endif; ?>
        </div>
    </div>
<?php endforeach; ?>
</div>
<?php endif; get_footer(); ?>
