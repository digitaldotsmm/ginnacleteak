<?php
define('TEMPLATE_URL', get_stylesheet_directory_uri());
define('ASSET_URL', TEMPLATE_URL . '/assets/');
require_once('includes/aq_resizer.php');

define('GT_HOME', 2);
define('GT_ABOUTUS', 50);
define('GT_TEAK', 6);
define('GT_INVESTMENT', 9);
define('GT_NATURE', 14);
define('GT_MANAGEMENT', 27);
define('GT_TYPE_PRODUCT', 'product');

#######################################################
/* * ********* CSS merge and minify process *********** */
#######################################################

if ($_SERVER['HTTP_HOST'] == 'teak.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
    require( 'includes/class.magic-min.php' );
    $minified = new Minifier(
            array(
        'echo' => false
            )
    );
    //exclude example
    $css_exclude = array(
            //TEMPLATEPATH . '/assets/css/bootstrap.css', 
            //TEMPLATEPATH . '/assets/css/flexslider.css'
    );
    //order example
    $css_order = array(
            //TEMPLATEPATH . '/assets/css/reset.css',     
    );

    $minified->merge(TEMPLATEPATH . '/assets/css/combine.min.css', TEMPLATEPATH . '/assets/css/combine', 'css', $css_exclude, $css_order);
}

function the_template_url() {
    echo TEMPLATE_URL;
}

################################################################################
// Enqueue Scripts
################################################################################

function init_scripts() {
    wp_deregister_script('wp-embed');
    wp_deregister_script('jquery');
    wp_deregister_script('comment-reply');
}

function add_scripts() {
    $js_path = ASSET_URL . 'js';
    $css_path = ASSET_URL . 'css';
    if ($_SERVER['HTTP_HOST'] == 'teak.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => $js_path . '/jquery.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'bootstrap',
                'src' => $js_path . '/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'bxslider',
                'src' => $js_path . '/jquery.bxslider.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'flexslider',
                'src' => $js_path . '/jquery.flexslider.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name'  => 'bootstrap',
                'src'   => $css_path . '/bootstrap.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name' => 'font-awesomes',
                'src' => $css_path . '/font-awesome.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),   
            array(
                'name' => 'flexslider',
                'src' => $css_path . '/flexslider.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),   
            array(
                'name' => 'bxslider',
                'src' => $css_path . '/jquery.bxslider.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),   
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
        );
    } else {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => $js_path . '/jquery.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'bootstrap',
                'src' => '//cdn.jsdelivr.net/bootstrap/3.3.6/js/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'bxslider',
                'src' => '//cdn.jsdelivr.net/bxslider/4.1.2/jquery.bxslider.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name'  => 'flexslider',
                'src'   => '//cdn.jsdelivr.net/flexslider/2.6.2/jquery.flexslider-min.js',
                'dep'   => 'jquery',
                'ver'   => null,
                'is_footer' => true
            ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name'  => 'bootstrap',
                'src'   => '//cdn.jsdelivr.net/bootstrap/3.3.6/css/bootstrap.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name'  => 'font-awesomes',
                'src'   => '//cdn.jsdelivr.net/fontawesome/4.6.1/css/font-awesome.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),            
             array(
                'name'  => 'flexslider',
                'src'   => '//cdn.jsdelivr.net/flexslider/2.6.2/flexslider.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),   
            array(
                'name'  => 'bxslider',
                'src'   => '//cdn.jsdelivr.net/bxslider/4.1.2/jquery.bxslider.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),   
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
        );
    }
    foreach ($js_libs as $lib) {
        wp_enqueue_script($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['is_footer']);
    }
    foreach ($css_libs as $lib) {
        wp_enqueue_style($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['media']);
    }
}

function my_deregister_scripts() {
    global $post_type;

    if (!is_front_page() && !is_home()) {
        
    }
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

function my_deregister_styles() {
    global $post_type;
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

if (!is_admin())
    add_action('init', 'init_scripts', 10);
add_action('wp_enqueue_scripts', 'add_scripts', 10);
add_action('wp_enqueue_scripts', 'my_deregister_scripts', 100);
add_action('wp_enqueue_scripts', 'my_deregister_styles', 100);



################################################################################
// Add theme support
################################################################################

add_theme_support('automatic-feed-links');
add_theme_support('nav-menus');
add_post_type_support('page', 'excerpt');
add_theme_support('post-thumbnails', array('post', 'page','product'));

if (function_exists('register_nav_menus')) {
    register_nav_menus(
            array(
                'main' => 'Main',
                'mobile' => 'Mobile',
            )
    );
}

################################################################################
// Add theme sidebars
################################################################################

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('Main - Sidebar'),
        'id' => 'main-sidebar-widget-area',
        'description' => 'Widgets in this area will be shown on the right sidebar of default page',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => '',
    ));
}
################################################################################
// Comment formatting
################################################################################

function theme_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li>
        <article <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
            <header class="comment-author vcard">
    <?php echo get_avatar($comment, $size = '48', $default = '<path_to_url>'); ?>
    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
                <time><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a></time>
                <?php edit_comment_link(__('(Edit)'), '  ', '') ?>
            </header>
                <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em>
                <br />
            <?php endif; ?>

    <?php comment_text() ?>

            <nav>
            <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </nav>
        </article>
        <!-- </li> is added by wordpress automatically -->
                <?php
            }
            