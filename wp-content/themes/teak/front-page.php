<?php get_header(); ?>
<div id="about" class="paddingtop20 paddingbottom20">
    <div class="row">
        <?php $title = get_field('title', GT_HOME, TRUE);
        if ($title): ?>
            <h1 class="text-center marginbottom30">
            <?php echo $title; ?>
            </h1>
        <?php endif; ?>
        <?php
        $page_meta = get_fields();        
        $all_pages = get_posts(array(
            'include' => array(GT_ABOUTUS, GT_NATURE, GT_MANAGEMENT),
            'post_type' => 'page',
            'orderby' => 'date',
            'order' => 'asc'
                ));
        if ($all_pages):
            foreach ($all_pages as $page):
                $title = $page->post_title;
                $link = get_permalink($page->ID);
                $img = get_attachment_image_src($page->ID, 'thumbnail');
                $mobile_img = get_attachment_image_src($page->ID, 'medium');
                //$resize_img =aq_resize($img[0],165,105,true,true,true);
                ?>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="about-list text-center">
                        <div class="list-img">
                            <img class="hidden-sm hidden-md hidden-lg" src="<?php echo $mobile_img[0]; ?>" alt="<?php echo $title; ?>">
                            <img class="hidden-xs img-circle" src="<?php echo $img[0]; ?>" alt="<?php echo $title; ?>">
                            <div class="mask">
                                <a href="<?php echo $link; ?>">
                                    <i class="fa fa-search"></i>
                                </a>
                            </div>
                        </div>
                        <h2 class="margintop20">
                            <a href="<?php echo $link; ?>">
        <?php echo $title; ?>
                            </a>
                        </h2>
                        <p>
        <?php echo $page->post_excerpt; ?>
                        </p>
                    </div>

                </div>
    <?php endforeach;
endif; ?>
    </div>
</div><!-- end about -->

<div id="featured">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="featured-title-wrap">
                <h2>
                    <img src="<?php echo ASSET_URL; ?>images/icon_featured.jpg" alt="icon_featured">
                    Featured Products
                </h2>
                <ul id="special-controls">
                    <li class="arr-up-down arr-up-down-prev"></li>
                    <li>|</li>
                    <li class="arr-up-down arr-up-down-next"></li>
                </ul><!-- end of special-controls -->
            </div>
        </div>
    </div>
    <?php
    $args = array(
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'ASC',
        'post_type' => GT_TYPE_PRODUCT,
        'post_status' => 'publish',
    );
    $products = get_posts($args);
    if ($products):
        ?>
        <ul id="featured-slider" class="row">
            <?php
            foreach ($products as $product):
                $title = $product->post_title;
                $link = get_permalink($product->ID);
                $img = get_attachment_image_src($product->ID, 'large');
                $resize_img = aq_resize($img[0], 260, 165, true, true, true);
                ?>
                <li class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="featured-list">
                        <div class="item-img">
                            <?php if ($img): ?>
                                <img class="img-responsive" src="<?php echo $resize_img; ?>" alt="<?php echo $title; ?>">
        <?php endif; ?>
                            <div class="mask">
                                <a href="<?php echo $link; ?>">
                                    <span>
                                        <i class="fa fa-eye">
                                            Quick View
                                        </i>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <h3 class="text-center">
                            <a href="<?php echo $link; ?>">
        <?php echo $title; ?>
                            </a>
                        </h3>
                    </div><!-- end featured-list -->
                </li>
        <?php endforeach; ?>
        </ul>
<?php endif; ?>
</div><!-- end featured -->

<div id="reference">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="notable-clients" class="row">
                <div class="col-md-12">
                    <div id="featured-title-wrap">
                        <h2>
                            <img src="http://teak.dd/wp-content/themes/teak/assets/images/icon_featured.jpg" alt="icon_featured">
                            Notable Clients                              
                        </h2>
                        <!--<span class="view-more">View All</span>-->
                    </div>
                    <div class="row">

                            <?php foreach ($page_meta['notable_clients'] as $client): ?>

                            <div class="single col-sm-4 col-md-2">
                                    <?php if ($client['client_website']): ?>
                                    <a href="http://<?php echo $client['client_website']; ?>" target="_blank" alt="<?php echo $client['client_name']; ?>">
                                    <?php endif; ?>

                                    <?php if ($client['client_logo']): ?>
                                        <img src="<?php echo $client['client_logo']['url']; ?>" />
    <?php endif; ?>
                                    <h3><?php echo $client['client_name']; ?></h3>                                
                                    <em><?php echo $client['note']; ?></em>

                                <?php if ($client['client_website']): ?>
                                    </a>
    <?php endif; ?>

                            </div>

<?php endforeach; ?>                            

                    </div>
                </div>
                <div id="testimonials" class="col-md-12">
                    
                    <div id="featured-title-wrap">
                        <h2>
                            <img src="http://teak.dd/wp-content/themes/teak/assets/images/icon_featured.jpg" alt="icon_featured">
                            Testimonials
                        </h2>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->                            
                            <ol class="carousel-indicators">
                                <?php  $i=0; foreach ( $page_meta['testimonials'] as $comment ) : ?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i==0) ? 'active': ''; ?>"></li>                                    
                                <?php $i++; endforeach; ?>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <?php $i=0; foreach ( $page_meta['testimonials'] as $comment ) : ?>
                                    <div class="item <?php echo ($i==0) ? 'active': ''; ?>">
                                        <p class="comment">
                                            <?php echo $comment['client_comment']; ?>
                                        </p>
                                        <p class="client-name"><?php echo $comment['client_name']; ?></p>
                                        <p class="client-position"><?php echo $comment['client_position']; ?></p>
                                        <p class="client-location"><?php echo $comment['client_location']; ?></p>
                                    </div>
                                <?php $i++; endforeach; ?>                               
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>                        

                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
